import { useFormik } from "formik";
import * as Yup from "yup";
import "./App.css";

const initialState = {
  fullname: "",
  email: "",
  password: "",
  status: "",
  is_active: false,
};

const validationSchema = Yup.object({
  fullname: Yup.string().required("Full name wajib diisi"),
  email: Yup.string().required("Email wajib diisi").email("Email tidak valid"),
  password: Yup.string()
    .required("Password wajib diisi")
    .min(8, "Minimal password adalah 8 huruf"),
  status: Yup.string().required("Status wajib diisi"),
  is_active: Yup.boolean(),
});

function App() {
  const onSubmit = () => {
    console.log(values);
    // mengirimkan data ke server
  };

  const { handleChange, values, handleSubmit, errors, handleBlur, touched } =
    useFormik({
      initialValues: initialState,
      onSubmit: onSubmit,
      validationSchema: validationSchema,
    });

  return (
    <div className="bg-gray-50 h-screen py-16">
      <section className="max-w-lg border-2 border-gray-500 mx-auto rounded-lg shadow-lg bg-white p-8">
        <h1 className="text-center text-2xl">Form</h1>
        <div className="flex flex-col gap-2">
          <label>Full Name</label>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            name="fullname"
            type="text"
            className="border border-gray-400 rounded-md p-1 pl-2"
            placeholder="Masukkan Nama Panjang"
          />
          <p className="text-red-600">
            {touched.fullname === true && errors.fullname}
          </p>
        </div>
        <div className="flex flex-col gap-2 mt-6">
          <label>Email</label>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            name="email"
            type="text"
            className="border border-gray-400 rounded-md p-1 pl-2"
            placeholder="Masukkan Email"
          />
          <p className="text-red-600">
            {touched.email === true && errors.email}
          </p>
        </div>
        <div className="flex flex-col gap-2 mt-6">
          <label>Password</label>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            name="password"
            type="password"
            className="border border-gray-400 rounded-md p-1 pl-2"
            placeholder="Masukkan Password"
          />
          <p className="text-red-600">
            {touched.password === true && errors.password}
          </p>
        </div>
        <div className="flex flex-col gap-2 mt-6">
          <label>Status</label>
          <select
            onChange={handleChange}
            onBlur={handleBlur}
            name="status"
            className="border border-gray-400 bg-white rounded-md p-1 pl-2"
          >
            <option value="">Pilih Status</option>
            <option value="free">Free</option>
            <option value="premium">Premium</option>
          </select>
          <p className="text-red-600">
            {touched.status === true && errors.status}
          </p>
        </div>
        <div className="flex flex-col gap-2 mt-6">
          <div className="flex item gap-2">
            <input
              onChange={handleChange}
              onBlur={handleBlur}
              name="is_active"
              type="checkbox"
            />
            <label htmlFor="">Is Active</label>
          </div>
          <p className="text-red-600">
            {touched.is_active === true && errors.is_active}
          </p>
        </div>
        <div className="my-4">
          <button
            onClick={handleSubmit}
            className="bg-blue-400 border-2 border-blue-400 w-full py-1 rounded-lg text-white"
          >
            Register
          </button>
        </div>
      </section>
    </div>
  );
}

export default App;
